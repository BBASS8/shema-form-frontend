import React, { useState, useEffect } from "react";

const FormComponent = () => {
  const [formData, setFormData] = useState("");

  const fetchData = async () => {
    const response = await fetch("http://localhost:8000/form/aa3795");
    const data = await response.json();
    console.log(response);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setFormData(data);
    }
  };

  useEffect(() => {
    console.log("cat");
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    fetchData();
  };

  return (
    <form onSubmit={handleSubmit}>
      <button type="submit">Submit</button>
      {formData && <pre>{JSON.stringify(formData, null, 2)}</pre>}
    </form>
  );
};

export default FormComponent;
